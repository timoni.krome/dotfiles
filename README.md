# BSPWM in KDE Plasma



## About The Project

This Project aims to make it easy to use the BSPWM Window Manager with KDE Plasma and smooth out some of the imperfections that come with that.

## Installation

### Dependencies

- BSPWM, SXHKD, picom, xcape

Install these using the package manager of your distro. (If you copy paste them from the readme you need to remove the comas)

### Actual Installation

Run the install.sh by opening the terminal inside the bspwm in kde folder and typing ./install.sh
