echo -ne "Warning: Back up your bspwm and sxhkd config, if you care about them"

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

read -p "Are you sure? [y/Y/j/J] " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[YyJj]$ ]]
then
	cp -r $SCRIPT_DIR/bspwm $HOME/.config
	cp -r $SCRIPT_DIR/sxhkd $HOME/.config
	sudo cp $SCRIPT_DIR/kde-bspwm-.desktop /usr/share/xsessions/kde-bspwm-.desktop
fi
